# LEM-IN

Avant-dernier projet d'algorithmie de 42.
Ce projet est une introduction a la theorie des graphs, comment les parcourir, les analyser, les stocker...
Le sujet detaille du projet est disponible [ici](https://cdn.intra.42.fr/pdf/pdf/1555/lem-in.fr.pdf).

## Fonctionnement

Ce projet nous demande de coder un programme prenant en entree un fichier decrivant une fourmiliere et donnant la
solution la plus rapide pour amener toutes les fourmies de la salle d'entree a la salle de sortie.
Le lem-in se lance de la facon suivante: `./lem-in < source_file`

### Le format du fichier source

La fourmiliere decrite dans le fichier source doit respecter un certain formatage:
1. La premiere ligne decrit **le nombre de fourmis** presentes dans la fourmiliere.
2. Viennent ensuite **la liste des differentes salles** avec leur nom et leurs coordonnes x et y.
- Les noms des salles peuvent etre composes de n'importe quel caractere imprimable de la table ascii **MAIS** ne
peuvent pas commencer par les caracteres '#' et 'L'.
- Les coordonnees x et y des salles sont toujours des nombre entiers.
3. Enfin, la liste des differentes **connexions** entre les salles.
4. **Deux commandes speciales** peuvent etre utilisees avant la description d'une salle et changent les proprietes de la
salle decrite ensuite:
- `##start` defini la salle suivante comme etant la salle d'entree de la fourmiliere
- `##end` defini la salle suivante comme etant la salle de sortie de la fourmiliere
5. Le fichier peut comporter **des commentaires** commencants par le caractere '#'.

Plus visuellement, un fichier source (ou "map") ressemble a ca:
```
nombre_de_fourmis
# salles
##start
salle_a Xa Ya
salle_b Xb Yb
...
##end
salle_p Xp Yp
# connexions
salle_a-salle_b
salle_b-salle_c
...
```

### Les regles

Une fois le formatage du fichier source compris, il faut maintenant parler des regles.
En effet, nos fourmies ne peuvent pas se teleporter de la salle "start" a la salle "end" ; elles doivent se deplacer
progressivement jusqu'a la sortie.
Les regles de deplacement sont les suivantes:
1. Toutes les fourmis sont conciderees comme etant dans la salle "start" au demarrage.
2. Une fourmis ne peut se deplacer qu'**une seule fois** par tour.
3. Une fourmis ne peut se deplacer dans une salle **uniquement si cette salle est vide**.
4. Les salles ne peuvent acceuillir qu'**une seule fourmis a la fois**. Les salles "start" et "end" ont cependant un
nombre de places **illimite**.
5. Nous pouvons deplacer **autant de fourmis que l'on veut et peut** par tour.
On concidere un tour comme etant une ligne de deplacement ecrite en solution.

### Le format de la solution

Notre programme doit donc ecrire la liste des deplacements des fourmis pour qu'elles se retrouvent toutes dans la salle
"end".
Dans un premier temps, le programme doit reecrire la description de la fourmiliere,
puis les deplacements de la forme suivante:
```
Lx-a Ly-b Lz-c ...
...
```
Avec x, y et z les numeros des fourmis (de 1 a nombre_de_fourmis), et a, b et c le nom de la salle dans laquelle se
deplace la fourmis.
Seules les fourmis se deplacant sont affichees.
Une ligne correspond a un tour. Donc, une solution faisant 15 lignes sera concideree plus rapide qu'une faisant 20
lignes.

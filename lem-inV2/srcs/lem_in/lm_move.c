/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_move.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 17:02:38 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/30 13:11:07 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_move_to_end(t_graph *graph, t_ptr *ptr, int *ant_end)
{
	ptr->room->full = 0;
	if (ptr->ant_id != -1)
	{
		if (graph->opts & (1 << LM_OPTS_C))
			ft_printf("%@L%i-%s%@ ", RED, \
			ptr->ant_id, ptr->next->room->name, EOC);
		else
			ft_printf("L%i-%s ", ptr->ant_id, ptr->next->room->name);
	}
	*ant_end += 1;
}

static void		lm_move_from_start(t_graph *graph, t_path *tmp, int *ant)
{
	if (tmp->total > 0 && *ant != graph->ant_members)
	{
		*ant += 1;
		tmp->total--;
		tmp->ptr->next->room->full = 1;
		tmp->ptr->next->ant_id = *ant;
		if (graph->opts & (1 << LM_OPTS_C))
			ft_printf("%@L%i-%s%@ ", GREEN, *ant, \
			tmp->ptr->next->room->name, EOC);
		else
			ft_printf("L%i-%s ", *ant, tmp->ptr->next->room->name);
	}
}

static void		lm_move_room(t_ptr *ptr)
{
	ptr->room->full = 0;
	ptr->next->room->full = 1;
	ptr->next->ant_id = ptr->ant_id;
	if (ptr->ant_id != -1)
		ft_printf("L%i-%s ", ptr->ant_id, ptr->next->room->name);
}

static void		lm_move_print_line(t_graph *graph, int line)
{
	if (graph->opts & (1 << LM_OPTS_C))
		ft_printf("%@#%.2d%@ ", YELLOW, line, EOC);
	else
		ft_printf("#%.2d ", line);
}

void			lm_move(t_graph *graph, int ant, int ant_end, int line)
{
	t_path			*tmp;
	t_ptr			*ptr;

	lm_unfill_rooms(graph);
	while (ant_end != graph->ant_members)
	{
		if (graph->opts & (1 << LM_OPTS_L))
			lm_move_print_line(graph, line++);
		tmp = graph->path_h.tail;
		while (tmp)
		{
			ptr = lm_go_to_end(tmp);
			while (ptr)
			{
				if (ptr->room->full && ptr->next->room->mod & (1 << LM_END))
					lm_move_to_end(graph, ptr, &ant_end);
				else if (ptr->room->full)
					lm_move_room(ptr);
				ptr = ptr->prev;
			}
			lm_move_from_start(graph, tmp, &ant);
			tmp = tmp->prev;
		}
		ft_putchar('\n');
	}
}

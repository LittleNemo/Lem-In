/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_ants.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 17:12:01 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 13:22:15 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_file			*lm_get_ants(t_file_h *file_h, t_graph *graph)
{
	t_file			*tmp;
	long			value;

	tmp = file_h->head;
	while (tmp->line[0] == '#')
		tmp = tmp->next;
	value = ft_atol(tmp->line);
	if (!value || value < 0 || \
		(ft_strlen(tmp->line) != (ft_numlen(value) + ft_isneg(value))))
		lm_error(4, file_h, NULL, NULL);
	graph->ant_members = value;
	tmp = tmp->next;
	return (tmp);
}

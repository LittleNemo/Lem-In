/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_create_to_end.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 13:19:55 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 22:50:35 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_ptr			*lm_find_in_path(t_room *room, t_graph *graph)
{
	t_path			*tmp_p;
	t_ptr			*tmp_r;

	tmp_p = graph->path_h.tail;
	while (tmp_p)
	{
		tmp_r = tmp_p->ptr;
		while (tmp_r)
		{
			if (tmp_r->room == room)
				return (tmp_r);
			tmp_r = tmp_r->next;
		}
		tmp_p = tmp_p->prev;
	}
	return (NULL);
}

static t_ptr	*lm_create_begining(t_ptr *curr, t_ptr **queue, \
		t_path *path, t_graph *graph)
{
	t_ptr			*fresh;
	t_ptr			*tmp;

	path = lm_new_path(curr, *queue, NULL, graph);
	while (curr->room)
	{
		if (!(fresh = (t_ptr *)malloc(sizeof(*fresh))))
			return (NULL);
		fresh->ant_id = -1;
		fresh->room = curr->room;
		fresh->next = path->ptr;
		if (path->ptr)
			path->ptr->prev = fresh;
		fresh->prev = NULL;
		path->ptr = fresh;
		curr->room = curr->room->parent;
	}
	path->gen = graph->gen;
	tmp = fresh;
	while (tmp->next)
		tmp = tmp->next;
	return (tmp);
}

static int		lm_create_end(t_ptr *tmp, t_room *end, t_ptr *link)
{
	t_ptr			*end_path;
	t_ptr			*fresh;

	while (link->next)
		link = link->next;
	end_path = NULL;
	while (link->room->name != end->parent->name)
	{
		if (!(fresh = (t_ptr *)malloc(sizeof(*fresh))))
			return (1);
		fresh->ant_id = -1;
		fresh->room = link->room;
		fresh->next = end_path;
		if (end_path)
			end_path->prev = fresh;
		fresh->prev = NULL;
		end_path = fresh;
		link = link->prev;
	}
	tmp->next = end_path;
	end_path->prev = tmp;
	return (0);
}

int				lm_create_to_end(t_ptr *curr, t_room *end, \
		t_ptr *queue, t_graph *graph)
{
	t_ptr			*tmp;
	t_ptr			*link;

	if (!(link = lm_find_in_path(end, graph)))
	{
		free(curr);
		lm_del_queue(queue);
		return (0);
	}
	if (!(tmp = lm_create_begining(curr, &queue, NULL, graph)))
	{
		free(curr);
		lm_del_queue(queue);
		lm_error(2, NULL, graph, NULL);
	}
	if (lm_create_end(tmp, end, link))
	{
		free(curr);
		lm_del_queue(queue);
		lm_error(2, NULL, graph, NULL);
	}
	lm_del_queue(queue);
	free(curr);
	return (1);
}

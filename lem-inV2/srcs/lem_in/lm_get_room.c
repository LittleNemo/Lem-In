/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_room.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 15:22:45 by lbrangie          #+#    #+#             */
/*   Updated: 2018/12/20 14:46:38 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_get_room_update(t_file_h *file_h, t_graph *graph, \
		char **tab, char mod)
{
	static size_t	id;
	t_room			*fresh;

	if (!(fresh = (t_room *)malloc(sizeof(*fresh))) || \
		!(fresh->name = ft_strdup(tab[0])))
		lm_error(2, file_h, graph, tab);
	fresh->id = ++id;
	fresh->coor_x = ft_atoi(tab[1]);
	fresh->coor_y = ft_atoi(tab[2]);
	fresh->mod = mod;
	fresh->full = 0;
	fresh->header = &graph->room_h;
	fresh->parent = NULL;
	lm_set_neig(&fresh->neig_h);
	fresh->next = NULL;
	if (!graph->room_h.head)
	{
		fresh->prev = NULL;
		graph->room_h.head = fresh;
		graph->room_h.tail = fresh;
		return ;
	}
	fresh->prev = graph->room_h.tail;
	graph->room_h.tail->next = fresh;
	graph->room_h.tail = fresh;
}

static int		lm_check_coor(char *coor_x, char *coor_y)
{
	long			vx;
	long			vy;

	vx = ft_atol(coor_x);
	vy = ft_atol(coor_y);
	if (ft_strlen(coor_x) != (ft_numlen(vx) + ft_isneg(vx)) || \
		ft_strlen(coor_y) != (ft_numlen(vy) + ft_isneg(vy)))
		return (1);
	return (0);
}

static t_file	*lm_check_room(t_file_h *file_h, t_graph *graph, \
		t_file *tmp, char mod)
{
	t_room			*found;
	char			**tab;

	if (!(tab = ft_strsplit(tmp->line, ' ')))
		lm_error(2, file_h, graph, NULL);
	if (ft_tablen(tab) != 3 || lm_check_coor(tab[1], tab[2]))
	{
		ft_freetab((void **)tab);
		return (NULL);
	}
	if (ft_strchr(tab[0], '-'))
		lm_error(8, file_h, graph, tab);
	if (lm_find_room(&graph->room_h, tab[0]))
		lm_error(9, file_h, graph, tab);
	if (mod == 1 && (found = lm_find_start(&graph->room_h)))
		found->mod = 0;
	if (mod == 2 && (found = lm_find_end(&graph->room_h)))
		found->mod = 0;
	lm_get_room_update(file_h, graph, tab, mod);
	ft_freetab((void **)tab);
	return (tmp);
}

t_file			*lm_get_room(t_file_h *file_h, t_graph *graph, t_file *tmp)
{
	lm_set_room(&graph->room_h);
	while (tmp && (ft_strpbrk(tmp->line, " #")))
	{
		if (ft_strequ(tmp->line, "##start") && \
			!(tmp = lm_check_room(file_h, graph, tmp->next, 1 << LM_START)))
			return (NULL);
		else if (ft_strequ(tmp->line, "##end") && \
			!(tmp = lm_check_room(file_h, graph, tmp->next, 1 << LM_END)))
			return (NULL);
		else if (tmp->line[0] != '#' && \
			!ft_strequ(tmp->prev->line, "##start") && \
			!ft_strequ(tmp->prev->line, "##end") && \
			!(tmp = lm_check_room(file_h, graph, tmp, 0)))
			return (NULL);
		tmp = tmp->next;
	}
	if (!graph->room_h.head)
		lm_error(5, file_h, graph, NULL);
	return (tmp);
}

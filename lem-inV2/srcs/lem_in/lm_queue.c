/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_queue.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 16:58:37 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/10 17:25:39 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_queue_push(t_ptr **queue, t_room *room, \
		t_file_h *file_h, t_graph *graph)
{
	t_ptr			*fresh;
	t_ptr			*tmp;

	if (!(fresh = (t_ptr *)malloc(sizeof(*fresh))))
		lm_error(2, file_h, graph, NULL);
	fresh->room = room;
	fresh->next = NULL;
	tmp = *queue;
	if (!*queue)
	{
		fresh->prev = NULL;
		*queue = fresh;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = fresh;
	fresh->prev = tmp;
}

t_ptr			*lm_queue_pop(t_ptr **queue)
{
	t_ptr			*head;

	if (!*queue)
		return (NULL);
	head = *queue;
	*queue = (*queue)->next;
	return (head);
}

void			lm_del_queue(t_ptr *queue)
{
	t_ptr			*tmp;

	while (queue)
	{
		tmp = queue;
		queue = queue->next;
		free(tmp);
	}
}

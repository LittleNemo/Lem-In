/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_sort_path.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 17:43:38 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/25 19:17:10 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_swap(t_path *to_swap)
{
	int				tmp_val;
	t_ptr			*tmp_ptr;

	tmp_val = to_swap->total;
	to_swap->total = to_swap->next->total;
	to_swap->next->total = tmp_val;
	tmp_val = to_swap->gen;
	to_swap->gen = to_swap->next->gen;
	to_swap->next->gen = tmp_val;
	tmp_val = to_swap->len_path;
	to_swap->len_path = to_swap->next->len_path;
	to_swap->next->len_path = tmp_val;
	tmp_val = to_swap->gen_tmp;
	to_swap->gen_tmp = to_swap->next->gen_tmp;
	to_swap->next->gen_tmp = tmp_val;
	tmp_val = to_swap->save;
	to_swap->save = to_swap->next->save;
	to_swap->next->save = tmp_val;
	tmp_ptr = to_swap->ptr;
	to_swap->ptr = to_swap->next->ptr;
	to_swap->next->ptr = tmp_ptr;
}

void			lm_sort_path(t_graph *graph)
{
	t_path			*tmp;

	tmp = graph->path_h.head;
	while (tmp->next)
	{
		if (tmp->len_path > tmp->next->len_path)
		{
			lm_swap(tmp);
			tmp = graph->path_h.head;
		}
		tmp = tmp->next;
	}
}

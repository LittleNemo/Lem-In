/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_set_data.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 17:06:29 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 13:15:48 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_set_file(t_file_h *file_h)
{
	file_h->head = NULL;
	file_h->tail = NULL;
}

void			lm_set_room(t_room_h *room_h)
{
	room_h->head = NULL;
	room_h->tail = NULL;
}

void			lm_set_neig(t_neig_h *neig_h)
{
	neig_h->head = NULL;
	neig_h->tail = NULL;
}

void			lm_set_path(t_path_h *path_h)
{
	path_h->head = NULL;
	path_h->tail = NULL;
}

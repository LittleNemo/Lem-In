/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_shim_lim.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: xmazella <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/15 11:22:16 by xmazella          #+#    #+#             */
/*   Updated: 2019/01/29 18:07:16 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int		init_gen(t_path *path, t_path *head, int gen, int *test)
{
	t_path	*tmp;
	int		i;

	i = 0;
	tmp = path;
	if (!tmp->next || (tmp->next && tmp->next->gen != tmp->gen))
		return (create_paquet(path, head, i, gen));
	else
	{
		while (tmp)
		{
			if (!tmp->next || (tmp->next->gen != tmp->gen))
				return (create_paquet(path, head, i, gen));
			i++;
			*test = i;
			tmp = tmp->next;
		}
	}
	return (0);
}

static int		lm_best_paquet(t_graph *graph, int i)
{
	float nb_tour;

	nb_tour = (float)((lm_sommes(graph->path_h.head, i)\
				+ graph->ant_members) / (1 + i));
	if (i == 0 || nb_tour <= graph->nb_tour)
	{
		lm_save(graph, i);
		graph->nb_tour = nb_tour;
		graph->best_gen = i;
		return (1);
	}
	return (0);
}

int				lm_shin_lim(t_graph *graph)
{
	t_path	*path;
	float	i;
	int		test;

	i = 0;
	test = 0;
	path = graph->path_h.head;
	len_path(path);
	while (i <= graph->gen && path && i < graph->ant_members)
	{
		if (init_gen(path, graph->path_h.head, i, &test) == 0)
			break ;
		if (lm_best_paquet(graph, i) == 0)
			break ;
		while (test-- > 0 && path)
			path = path->next;
		if (path)
			path = path->next;
		i++;
	}
	lm_recovery(graph, graph->best_gen);
	return (0);
}

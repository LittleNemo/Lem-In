/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_simulation.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: xmazella <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 13:05:05 by xmazella          #+#    #+#             */
/*   Updated: 2019/01/29 14:35:49 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			lm_reste(t_graph *graph, int reste)
{
	t_path	*path;
	int		save_len;
	int		nb;

	path = graph->path_h.head;
	if (path)
		save_len = path->len_path;
	while (path)
	{
		nb = path->len_path - save_len;
		if ((nb == 0 || nb >= reste) && reste > 0)
		{
			path->total++;
			reste--;
		}
		path = path->next;
	}
	if (reste > 0)
		lm_reste(graph, reste);
	return (0);
}

int			lm_simulation(t_graph *graph)
{
	t_path	*path;
	int		nb_ants;
	float	nb_tour;

	path = graph->path_h.head;
	nb_tour = graph->nb_tour;
	nb_ants = graph->ant_members;
	while (path && nb_ants > 0)
	{
		path->total = (int)nb_tour - path->len_path;
		if (path->total < 0)
			path->total = 0;
		nb_ants -= path->total;
		path = path->next;
	}
	if (nb_ants > 0)
		lm_reste(graph, nb_ants);
	return (1);
}

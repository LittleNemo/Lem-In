/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_del_data.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 16:47:00 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 13:16:38 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_del_file(t_file_h *file_h)
{
	t_file			*tmp;

	file_h->tail = NULL;
	while (file_h->head)
	{
		tmp = file_h->head;
		file_h->head = file_h->head->next;
		ft_strdel(&tmp->line);
		free(tmp);
	}
}

void			lm_del_room(t_room_h *room_h)
{
	t_room			*tmp;

	room_h->tail = NULL;
	while (room_h->head)
	{
		tmp = room_h->head;
		room_h->head = room_h->head->next;
		ft_strdel(&tmp->name);
		if (tmp->neig_h.head)
			lm_del_neig(&tmp->neig_h);
		free(tmp);
	}
}

void			lm_del_neig(t_neig_h *neig_h)
{
	t_neig			*tmp;

	neig_h->tail = NULL;
	while (neig_h->head)
	{
		tmp = neig_h->head;
		neig_h->head = neig_h->head->next;
		free(tmp);
	}
}

void			lm_del_path(t_path_h *path_h)
{
	t_path			*tmp;
	t_ptr			*tmp_ptr;

	path_h->tail = NULL;
	while (path_h->head)
	{
		tmp = path_h->head;
		path_h->head = path_h->head->next;
		while (tmp->ptr)
		{
			tmp_ptr = tmp->ptr;
			tmp->ptr = tmp->ptr->next;
			free(tmp_ptr);
		}
		free(tmp);
	}
}

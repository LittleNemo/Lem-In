/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 10:36:56 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/22 18:24:12 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_put_path_loop(t_ptr *tmp)
{
	if (tmp->room->mod & (1 << LM_START))
		ft_printf("%@%s%@", GREEN, tmp->room->name, EOC);
	else if (tmp->room->mod & (1 << LM_END))
		ft_printf("%@%s%@", RED, tmp->room->name, EOC);
	else
		ft_printf("%s", tmp->room->name);
	if (tmp->next)
		ft_putstr(" -> ");
}

void			lm_put_path(t_path_h *path_h)
{
	t_path			*path;
	t_ptr			*tmp;
	size_t			nb_room;
	size_t			i;

	path = path_h->head;
	i = 0;
	ft_putchar('\n');
	while (path)
	{
		ft_printf("PATH #%.2d: ", ++i);
		tmp = path->ptr;
		nb_room = 0;
		while (tmp)
		{
			lm_put_path_loop(tmp);
			nb_room++;
			tmp = tmp->next;
		}
		ft_printf(" (%@%zu%@)\n", LIGHT_BLUE, path->len_path, EOC);
		path = path->next;
	}
}

void			reset_move(t_graph *graph)
{
	t_path	*path;
	int		gen;
	int		i;

	i = 0;
	path = graph->path_h.head;
	gen = graph->best_gen;
	while (path)
	{
		if (path->gen_tmp == gen && i <= gen)
		{
			i++;
			path = path->next;
			continue ;
		}
		else
			path->gen_tmp = -1;
		path = path->next;
	}
}

int				main(int ac, char **av)
{
	t_file_h		file_h;
	t_graph			graph;

	ft_bzero(&graph, sizeof(t_graph));
	ft_bzero(&file_h, sizeof(t_file_h));
	lm_get_opts(av + 1, &graph);
	lm_error(!!(ac - 1 - graph.nb_opts), NULL, NULL, NULL);
	lm_get_file(&file_h);
	lm_get_data(&file_h, &graph);
	lm_set_remain(&file_h, &graph);
	if (lm_solve(&file_h, &graph))
	{
		lm_shin_lim(&graph);
		reset_move(&graph);
		lm_purge_path(&graph);
		lm_sort_path(&graph);
		lm_simulation(&graph);
		lm_move(&graph, 0, 0, 1);
	}
	if (graph.opts & (1 << LM_OPTS_P))
		lm_put_path(&graph.path_h);
	lm_del_room(&graph.room_h);
	lm_del_path(&graph.path_h);
	return (0);
}

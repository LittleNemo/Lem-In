/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_solve.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 13:12:43 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 22:50:48 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_set_full(t_neig *tmp, t_room *parent, \
		t_ptr **queue, t_graph *graph)
{
	tmp->room->parent = parent;
	tmp->room->full = 1;
	lm_queue_push(queue, tmp->room, NULL, graph);
}

int				lm_backward(t_room *back, t_ptr *queue, t_graph *graph)
{
	t_neig			*tmp;
	t_room			*copy;

	copy = back;
	while (lm_islock(copy) && !(copy->mod & (1 << LM_START)))
		copy = copy->parent;
	if (copy->mod & (1 << LM_START))
		return (0);
	tmp = copy->neig_h.head;
	while (tmp)
	{
		if (!tmp->room->full)
			lm_set_full(tmp, copy, &queue, graph);
		tmp = tmp->next;
	}
	return (lm_bfs(copy, queue, NULL, graph));
}

int				lm_bfs(t_room *parent, t_ptr *queue, \
		t_ptr *curr, t_graph *graph)
{
	t_neig			*tmp;

	lm_queue_push(&queue, graph->room_h.start, NULL, graph);
	while (queue)
	{
		curr = lm_queue_pop(&queue);
		!parent ? curr->room->parent = parent : 0;
		if (curr->room->mod & (1 << LM_END))
			return (lm_create_path(curr, queue, graph));
		parent = curr->room;
		tmp = curr->room->neig_h.head;
		while (tmp)
		{
			if (!tmp->room->full)
				lm_set_full(tmp, parent, &queue, graph);
			else if (lm_islock(curr->room) && lm_islock(tmp->room) && \
		tmp->room != curr->room->parent && lm_find_in_path(tmp->room, graph) &&\
		lm_backward(tmp->room, NULL, graph))
				return (lm_create_to_end(curr, tmp->room, queue, graph));
			tmp = tmp->next;
		}
		free(curr);
	}
	return (0);
}

static int		lm_straight(t_file_h *file_h, t_graph *graph)
{
	int				ant;
	t_neig			*tmp;

	tmp = graph->room_h.start->neig_h.head;
	while (tmp)
	{
		if (tmp->room == graph->room_h.end)
		{
			ant = 1;
			lm_put_file(file_h);
			while (ant <= graph->ant_members)
				ft_printf("L%i-%s ", ant++, graph->room_h.end->name);
			ft_putchar('\n');
			return (1);
		}
		tmp = tmp->next;
	}
	return (0);
}

int				lm_solve(t_file_h *file_h, t_graph *graph)
{
	int				max_path;

	if (lm_straight(file_h, graph))
		return (0);
	lm_set_path(&graph->path_h);
	graph->room_h.start->full = 1;
	lm_first_bfs(NULL, NULL, file_h, graph);
	lm_put_file(file_h);
	lm_reset_map(graph, graph->room_h.head);
	graph->gen = 0;
	max_path = lm_max_path(&graph->room_h);
	while (graph->gen < max_path && !lm_islock(graph->room_h.start) && \
		!lm_islock(graph->room_h.end))
	{
		graph->room_h.start->full = 1;
		graph->gen++;
		lm_bfs(NULL, NULL, NULL, graph);
		lm_reset_map(graph, graph->room_h.head);
	}
	return (1);
}

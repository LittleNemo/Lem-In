/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_recovery.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: xmazella <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/29 14:08:39 by xmazella          #+#    #+#             */
/*   Updated: 2019/01/29 14:15:11 by xmazella         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		lm_save(t_graph *graph, int gen)
{
	t_path *path;

	path = graph->path_h.head;
	while (path)
	{
		if (path->gen_tmp == gen)
			path->save = gen;
		path = path->next;
	}
}

void		lm_recovery(t_graph *graph, int gen)
{
	t_path *path;

	path = graph->path_h.head;
	while (path)
	{
		if (path->save == gen)
			path->gen_tmp = path->save;
		path = path->next;
	}
}

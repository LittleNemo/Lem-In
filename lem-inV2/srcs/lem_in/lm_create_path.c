/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_create_path.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/20 15:59:32 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/22 17:27:56 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_path			*lm_new_path(t_ptr *curr, t_ptr *queue, \
		t_file_h *file_h, t_graph *graph)
{
	t_path			*fresh;

	if (!(fresh = (t_path *)malloc(sizeof(*fresh))))
	{
		free(curr);
		lm_del_queue(queue);
		lm_error(2, file_h, graph, NULL);
	}
	fresh->ptr = NULL;
	fresh->header = &graph->path_h;
	fresh->next = NULL;
	if (!graph->path_h.head)
	{
		fresh->prev = NULL;
		graph->path_h.head = fresh;
		graph->path_h.tail = fresh;
		return (graph->path_h.tail);
	}
	fresh->prev = graph->path_h.tail;
	graph->path_h.tail->next = fresh;
	graph->path_h.tail = fresh;
	return (graph->path_h.tail);
}

int				lm_create_path(t_ptr *curr, t_ptr *queue, t_graph *graph)
{
	t_ptr			*fresh;
	t_path			*path;

	path = lm_new_path(curr, queue, NULL, graph);
	while (curr->room)
	{
		if (!(fresh = (t_ptr *)malloc(sizeof(*fresh))))
		{
			free(curr);
			lm_del_queue(queue);
			lm_error(2, NULL, graph, NULL);
		}
		fresh->ant_id = -1;
		fresh->room = curr->room;
		fresh->next = path->ptr;
		if (path->ptr)
			path->ptr->prev = fresh;
		fresh->prev = NULL;
		path->ptr = fresh;
		curr->room = curr->room->parent;
	}
	path->gen = graph->gen;
	free(curr);
	lm_del_queue(queue);
	return (1);
}

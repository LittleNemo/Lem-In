/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_shim_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: xmazella <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/29 14:15:34 by xmazella          #+#    #+#             */
/*   Updated: 2019/01/29 14:26:34 by xmazella         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			lm_sommes(t_path *head, int gen)
{
	float		len;

	len = 0;
	while (head)
	{
		if (head->gen_tmp == gen)
			len += head->len_path;
		head = head->next;
	}
	return (len);
}

int			count_path(t_path *path, int gen)
{
	int		i;
	t_path	*tmp;

	i = 0;
	tmp = path;
	while (tmp)
	{
		if (tmp->gen_tmp == gen)
			i++;
		tmp = tmp->next;
	}
	return (i);
}

void		len_path(t_path *path)
{
	t_ptr	*tmp;
	int		i;

	while (path)
	{
		i = 0;
		tmp = path->ptr;
		while (tmp)
		{
			i++;
			tmp = tmp->next;
		}
		path->gen_tmp = -1;
		path->save = 0;
		path->total = 0;
		path->len_path = i;
		path = path->next;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_purge_path.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 14:42:27 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/25 16:12:34 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_purge(t_ptr *to_purge)
{
	t_ptr			*tmp;

	while (to_purge)
	{
		tmp = to_purge;
		to_purge = to_purge->next;
		free(tmp);
	}
}

void			lm_purge_path(t_graph *graph)
{
	t_path			*tmp;
	t_path			*to_purge;

	tmp = graph->path_h.head;
	while (tmp)
	{
		if (tmp->gen_tmp == -1)
		{
			to_purge = tmp;
			if (tmp->prev)
				tmp->prev->next = tmp->next;
			else
				graph->path_h.head = tmp->next;
			if (tmp->next)
				tmp->next->prev = tmp->prev;
			else
				graph->path_h.tail = tmp->prev;
			lm_purge(to_purge->ptr);
			if (tmp->prev)
				tmp = tmp->prev;
			free(to_purge);
		}
		tmp = tmp->next;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 16:29:26 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 13:21:47 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_error_end(t_file_h *file_h, t_graph *graph, char **tab)
{
	if (file_h)
		lm_del_file(file_h);
	if (graph && graph->room_h.head)
		lm_del_room(&graph->room_h);
	if (graph && graph->path_h.head)
		lm_del_path(&graph->path_h);
	if (tab)
		ft_freetab((void **)tab);
	exit(EXIT_FAILURE);
}

static void		lm_error_part2(int code)
{
	if (code == 11)
		ft_fprintf(stderr, "%s\n", LM_NO_PATH);
	else if (code == 12)
		ft_fprintf(stderr, "%s\n", LM_READ);
}

void			lm_error(int code, t_file_h *file_h, t_graph *graph, char **tab)
{
	if (code == 0)
		return ;
	else if (code == 1)
		ft_fprintf(stderr, "%s\n", LM_USAGE);
	else if (code == 2)
		ft_fprintf(stderr, "%s\n", LM_MALLOC);
	else if (code == 3)
		ft_fprintf(stderr, "%s\n", LM_EMPTY);
	else if (code == 4)
		ft_fprintf(stderr, "%s\n", LM_NO_ANT);
	else if (code == 5)
		ft_fprintf(stderr, "%s\n", LM_NO_ROOM);
	else if (code == 6)
		ft_fprintf(stderr, "%s\n", LM_NO_START);
	else if (code == 7)
		ft_fprintf(stderr, "%s\n", LM_NO_END);
	else if (code == 8)
		ft_fprintf(stderr, "%s\n", LM_ROOM_AMB);
	else if (code == 9)
		ft_fprintf(stderr, "%s\n", LM_ROOM_DUP);
	else if (code == 10)
		ft_fprintf(stderr, "%s\n", LM_ROOM_UKW);
	else
		lm_error_part2(code);
	lm_error_end(file_h, graph, tab);
}

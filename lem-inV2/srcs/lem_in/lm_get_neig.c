/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_neig.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 13:37:41 by lbrangie          #+#    #+#             */
/*   Updated: 2018/11/28 15:51:48 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_get_neig_update_b(t_file_h *file_h, t_graph *graph, \
		char **tab, t_room *room)
{
	static size_t	id;
	t_neig			*fresh;

	if (!(fresh = (t_neig *)malloc(sizeof(*fresh))))
		lm_error(2, file_h, graph, tab);
	fresh->id = ++id;
	fresh->room = lm_find_room(&graph->room_h, tab[0]);
	fresh->header = &room->neig_h;
	fresh->next = NULL;
	if (!room->neig_h.head)
	{
		fresh->prev = NULL;
		room->neig_h.head = fresh;
		room->neig_h.tail = fresh;
		return ;
	}
	fresh->prev = room->neig_h.tail;
	room->neig_h.tail->next = fresh;
	room->neig_h.tail = fresh;
}

static void		lm_get_neig_update_a(t_file_h *file_h, t_graph *graph, \
		char **tab, t_room *room)
{
	static size_t	id;
	t_neig			*fresh;

	if (!(fresh = (t_neig *)malloc(sizeof(*fresh))))
		lm_error(2, file_h, graph, tab);
	fresh->id = ++id;
	fresh->room = lm_find_room(&graph->room_h, tab[1]);
	fresh->header = &room->neig_h;
	fresh->next = NULL;
	if (!room->neig_h.head)
	{
		fresh->prev = NULL;
		room->neig_h.head = fresh;
		room->neig_h.tail = fresh;
		return ;
	}
	fresh->prev = room->neig_h.tail;
	room->neig_h.tail->next = fresh;
	room->neig_h.tail = fresh;
}

static t_file	*lm_check_neig(t_file_h *file_h, t_graph *graph, t_file *tmp)
{
	char			**tab;
	t_room			*room_a;
	t_room			*room_b;

	room_a = NULL;
	room_b = NULL;
	if (!(tab = ft_strsplit(tmp->line, '-')))
		lm_error(2, file_h, graph, NULL);
	if (ft_tablen(tab) != 2)
	{
		ft_freetab((void **)tab);
		return (NULL);
	}
	if (!(room_a = lm_find_room(&graph->room_h, tab[0])) || \
		!(room_b = lm_find_room(&graph->room_h, tab[1])))
		lm_error(10, file_h, graph, tab);
	lm_get_neig_update_a(file_h, graph, tab, room_a);
	lm_get_neig_update_b(file_h, graph, tab, room_b);
	ft_freetab((void **)tab);
	return (tmp);
}

t_file			*lm_get_neig(t_file_h *file_h, t_graph *graph, t_file *tmp)
{
	while (tmp && ft_strpbrk(tmp->line, "-#"))
	{
		if (tmp->line[0] != '#' && !(tmp = lm_check_neig(file_h, graph, tmp)))
			return (NULL);
		tmp = tmp->next;
	}
	return (tmp);
}

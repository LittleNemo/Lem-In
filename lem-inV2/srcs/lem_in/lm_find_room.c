/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_find_room.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 16:55:08 by lbrangie          #+#    #+#             */
/*   Updated: 2018/12/20 16:33:57 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_room			*lm_find_room(t_room_h *room_h, char *name)
{
	t_room			*tmp;

	tmp = room_h->head;
	while (tmp)
	{
		if (ft_strequ(tmp->name, name))
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

t_room			*lm_find_start(t_room_h *room_h)
{
	t_room			*tmp;

	tmp = room_h->head;
	while (tmp)
	{
		if (tmp->mod & (1 << LM_START))
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

t_room			*lm_find_end(t_room_h *room_h)
{
	t_room			*tmp;

	tmp = room_h->head;
	while (tmp)
	{
		if (tmp->mod & (1 << LM_END))
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

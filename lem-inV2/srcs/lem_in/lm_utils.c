/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 14:50:54 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 22:28:33 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int				lm_islock(t_room *room)
{
	t_neig			*tmp;

	tmp = room->neig_h.head;
	while (tmp)
	{
		if (tmp->room->mod)
		{
			tmp = tmp->next;
			continue ;
		}
		if (!tmp->room->full)
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

void			lm_reset_map(t_graph *graph, t_room *tmp_r)
{
	t_path			*tmp_p;
	t_ptr			*tmp;

	while (tmp_r)
	{
		tmp_r->full = 0;
		tmp_r->parent = NULL;
		tmp_r = tmp_r->next;
	}
	tmp_p = graph->path_h.head;
	while (tmp_p)
	{
		tmp = tmp_p->ptr;
		while (tmp->next)
			tmp = tmp->next;
		while (tmp->prev)
		{
			tmp->room->parent = tmp->prev->room;
			tmp->room->full = 1;
			tmp = tmp->prev;
		}
		tmp_p = tmp_p->next;
	}
	graph->room_h.end->full = 0;
}

int				lm_max_path(t_room_h *room_h)
{
	int				start;
	int				end;
	t_neig			*tmp;

	start = 0;
	tmp = room_h->start->neig_h.head;
	while (tmp)
	{
		start++;
		tmp = tmp->next;
	}
	end = 0;
	tmp = room_h->end->neig_h.head;
	while (tmp)
	{
		end++;
		tmp = tmp->next;
	}
	if (start < end)
		return (start);
	return (end);
}

void			lm_unfill_rooms(t_graph *graph)
{
	t_room			*tmp;

	tmp = graph->room_h.head;
	while (tmp)
	{
		tmp->full = 0;
		tmp = tmp->next;
	}
}

t_ptr			*lm_go_to_end(t_path *path)
{
	t_ptr			*tmp;

	tmp = path->ptr;
	while (tmp->next->next)
		tmp = tmp->next;
	return (tmp);
}

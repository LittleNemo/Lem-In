/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_file.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 16:44:22 by lbrangie          #+#    #+#             */
/*   Updated: 2018/12/26 17:08:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_get_file_update(t_file_h *file_h, char *line)
{
	static size_t	id;
	t_file			*fresh;

	if (!(fresh = (t_file *)malloc(sizeof(*fresh))))
		lm_error(2, file_h, NULL, NULL);
	if (!(fresh->line = ft_strdup(line)))
		lm_error(2, file_h, NULL, NULL);
	fresh->id = ++id;
	fresh->header = file_h;
	fresh->next = NULL;
	if (!file_h->head)
	{
		fresh->prev = NULL;
		file_h->head = fresh;
		file_h->tail = fresh;
		return ;
	}
	fresh->prev = file_h->tail;
	file_h->tail->next = fresh;
	file_h->tail = fresh;
}

void			lm_get_file(t_file_h *file_h)
{
	char			*line;

	line = NULL;
	if (read(0, line, stdin->_file) == -1)
		lm_error(12, NULL, NULL, NULL);
	lm_set_file(file_h);
	while (get_next_line(stdin->_file, &line))
	{
		lm_get_file_update(file_h, line);
		ft_strdel(&line);
	}
	ft_strdel(&line);
	if (!file_h->head)
		lm_error(3, NULL, NULL, NULL);
}

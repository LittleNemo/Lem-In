/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_data.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 14:55:08 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 13:23:50 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_set_remain(t_file_h *file_h, t_graph *graph)
{
	if (!(graph->room_h.start = lm_find_start(&graph->room_h)))
		lm_error(6, file_h, graph, NULL);
	if (!(graph->room_h.end = lm_find_end(&graph->room_h)))
		lm_error(7, file_h, graph, NULL);
}

void			lm_get_data(t_file_h *file_h, t_graph *graph)
{
	t_file			*tmp;

	if (!(tmp = lm_get_ants(file_h, graph)))
		return ;
	if (!(tmp = lm_get_room(file_h, graph, tmp)))
		return ;
	if (!(tmp = lm_get_neig(file_h, graph, tmp)))
		return ;
}

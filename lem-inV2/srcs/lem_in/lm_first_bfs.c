/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_first_bfs.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 17:01:29 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 17:37:06 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_create_shortest(t_ptr *curr, t_ptr *queue, \
		t_file_h *file_h, t_graph *graph)
{
	t_ptr			*fresh;
	t_path			*path;

	path = lm_new_path(curr, queue, file_h, graph);
	while (curr->room)
	{
		if (!(fresh = (t_ptr *)malloc(sizeof(*fresh))))
		{
			free(curr);
			lm_del_queue(queue);
			lm_error(2, file_h, graph, NULL);
		}
		fresh->room = curr->room;
		fresh->next = path->ptr;
		if (path->ptr)
			path->ptr->prev = fresh;
		fresh->prev = NULL;
		path->ptr = fresh;
		curr->room = curr->room->parent;
	}
	path->gen = 0;
	free(curr);
	lm_del_queue(queue);
}

void			lm_first_bfs(t_room *parent, t_ptr *queue, \
		t_file_h *file_h, t_graph *graph)
{
	t_ptr			*curr;
	t_neig			*tmp;

	lm_queue_push(&queue, graph->room_h.start, file_h, graph);
	while (queue)
	{
		curr = lm_queue_pop(&queue);
		!parent ? curr->room->parent = parent : 0;
		if (curr->room->mod & (1 << LM_END))
			return (lm_create_shortest(curr, queue, file_h, graph));
		parent = curr->room;
		tmp = curr->room->neig_h.head;
		free(curr);
		while (tmp)
		{
			if (!tmp->room->full)
			{
				tmp->room->parent = parent;
				tmp->room->full = 1;
				lm_queue_push(&queue, tmp->room, file_h, graph);
			}
			tmp = tmp->next;
		}
	}
	lm_error(11, file_h, graph, NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_create_paquet.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: xmazella <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/29 14:01:18 by xmazella          #+#    #+#             */
/*   Updated: 2019/01/29 19:57:59 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int		reset_path(t_path *path, int gen)
{
	t_path *tmp;

	tmp = path;
	while (tmp)
	{
		if (tmp->gen_tmp == gen - 1)
			tmp->gen_tmp = gen;
		tmp = tmp->next;
	}
	return (0);
}

static int		check_path(t_ptr *ptr, t_path *path, int nb_path)
{
	t_ptr	*ptr_save;
	t_ptr	*path_ptr;

	while (path && nb_path--)
	{
		path_ptr = path->ptr;
		while (path_ptr)
		{
			ptr_save = ptr;
			while (ptr)
			{
				if ((ptr->room->name == path_ptr->room->name)\
						&& !(ptr->room->mod))
					return (0);
				ptr = ptr->next;
			}
			path_ptr = path_ptr->next;
			ptr = ptr_save;
		}
		path = path->next;
	}
	return (1);
}

static int		remove_path(t_path *head, t_path *path, int nb_path, int gen)
{
	int				i;

	i = 0;
	while (head && head->gen < gen)
	{
		if (head->gen_tmp == gen - 1 &&\
				check_path(head->ptr, path, nb_path) == 1)
		{
			head->gen_tmp = gen;
			if ((++i + nb_path) == gen)
				return (0);
		}
		head = head->next;
	}
	return (0);
}

static int		add_simple_path(t_path *path, t_path *head, int gen)
{
	path->gen_tmp = gen;
	if (count_path(head, gen) == gen + 1)
		return (1);
	else
	{
		reset_path(head, gen);
		if (count_path(head, gen) == gen + 1)
			return (1);
	}
	return (0);
}

int				create_paquet(t_path *path, t_path *head, int nb_path, int gen)
{
	t_path	*tmp;
	int		i;

	tmp = path;
	i = -1;
	if (nb_path == 0)
		return (add_simple_path(path, head, gen));
	else
	{
		while (++i <= nb_path)
		{
			tmp->gen_tmp = gen;
			tmp = tmp->next;
		}
		tmp = path;
		if (count_path(head, gen) == gen + 1)
			return (1);
		else
		{
			remove_path(head, path, nb_path, gen);
			if (count_path(head, gen) == gen + 1)
				return (1);
		}
	}
	return (0);
}

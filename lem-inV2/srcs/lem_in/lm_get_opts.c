/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_opts.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/29 13:47:15 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 14:11:12 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_get_opts(char **av, t_graph *graph)
{
	int				i;
	int				j;

	i = -1;
	graph->opts = 0;
	graph->nb_opts = 0;
	while (av[++i] && av[i][0] == '-' && ft_str_isalpha(av[i] + 1))
	{
		j = 0;
		while (av[i][++j])
			if (av[i][j] == 'c')
				graph->opts |= (1 << LM_OPTS_C);
			else if (av[i][j] == 'l')
				graph->opts |= (1 << LM_OPTS_L);
			else if (av[i][j] == 'p')
				graph->opts |= (1 << LM_OPTS_P);
			else
				return ;
		graph->nb_opts++;
	}
}

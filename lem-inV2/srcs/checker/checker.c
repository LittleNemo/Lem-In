/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 10:38:23 by lbrangie          #+#    #+#             */
/*   Updated: 2018/11/16 15:17:42 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void		ck_print_result(size_t expected, size_t ans_size)
{
	ft_printf("There are %@%zu%@ lines expected.\n", BOLD, expected, EOC);
	ft_printf("Your answer is %@%zu%@ lines long.\n", BOLD, ans_size, EOC);
	if (ans_size < expected)
		ft_printf("%@OU%@TS%@TA%@ND%@IN%@G!%@!!%@\n", \
		RED, ORANGE, YELLOW, GREEN, LIGHT_BLUE, BLUE, PURPLE, EOC);
	else if (ans_size == expected)
		ft_printf("%@OK++%@\n", GREEN, EOC);
	else if (ans_size == expected + 1)
		ft_printf("%@OK+%@\n", LIGHT_BLUE, EOC);
	else if (ans_size == expected + 2)
		ft_printf("%@OK%@\n", YELLOW, EOC);
	else if (ans_size == expected + 3)
		ft_printf("%@OK-%@\n", ORANGE, EOC);
	else if (ans_size > expected + 3)
		ft_printf("%@KO%@\n", RED, EOC);
}

static int		ck_strdel(char **as)
{
	if (!as || !*as)
		return (0);
	free(*as);
	*as = NULL;
	return (1);
}

static void		ck_error(int code, char *line, int fd)
{
	if (code == 1)
		return ;
	if (code == 2)
		ft_putstr_endl("Error (checker): open failure");
	if (code == 3)
		ft_putstr_endl("Error (checker): number of lines expected no found");
	else
		ft_putstr_endl("usage: ./checker source_file");
	while (ck_strdel(&line) && get_next_line(fd, &line))
		;
	close(fd);
	exit(EXIT_FAILURE);
}

int				main(int ac, char **av)
{
	int				fd;
	char			*line;
	size_t			expected;
	size_t			ans_size;
	char			*ptr;

	ck_error(ac - 1, NULL, 0);
	if ((fd = open(av[1], O_RDONLY)) == -1)
		ck_error(2, NULL, 0);
	get_next_line(fd, &line);
	ck_strdel(&line);
	get_next_line(fd, &line);
	if (!(ptr = ft_strpbrk(line, "0123456789")))
		ck_error(3, line, fd);
	expected = ft_atoi(ptr);
	ck_strdel(&line);
	while (get_next_line(fd, &line) && *line)
		ck_strdel(&line);
	ans_size = 0;
	while (ck_strdel(&line) && get_next_line(fd, &line))
		ans_size++;
	ck_strdel(&line);
	close(fd);
	ck_print_result(expected, ans_size);
	return (0);
}

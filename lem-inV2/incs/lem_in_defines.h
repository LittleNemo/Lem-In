/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in_defines.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:22:58 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/30 13:11:58 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_DEFINES_H
# define LEM_IN_DEFINES_H

# define LM_USAGE		"usage ./lem-in [-lc] [< source_file]"
# define LM_MALLOC		"Error (lem-in): malloc failed"
# define LM_READ		"Error (lem-in): can not read file"

# define LM_EMPTY		"Error (lem-in): empty file"
# define LM_NO_ANT		"Error (lem-in): wrong ant number"
# define LM_NO_END		"Error (lem-in): no end"
# define LM_NO_ROOM		"Error (lem-in): no room"
# define LM_NO_START	"Error (lem-in): no start"
# define LM_NO_PATH		"Error (lem-in): no path"

# define LM_ROOM_DUP	"Error (lem-in): duplicate room name"
# define LM_ROOM_AMB	"Error (lem-in): ambiguous room name"
# define LM_ROOM_UKW	"Error (lem-in): unknown room name"

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in_typedefs.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:22:59 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/30 13:11:49 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_TYPEDEFS_H
# define LEM_IN_TYPEDEFS_H

/*
**	In this file, there is every structure used in the lem-in project. Those
**	structure are mostly used in linked lists.
**	The structure prefixed by "_h" are a header of a liked list. Each header
**	contain, a pointer to the head of the list and one to the tail. Those
**	headers speed up a bit the aquisition and the creation of the lists.
**	Each link of the lists contain a pointer to the header of list there
**	represent, a pointer to the next link and on to the previous one.
*/

/*
**		S_FILE:
**	The s_file structure contains the informations of each lines of the source
**	file.
**		- The size_t id is the identifier of a given line of the file.
**		- The char *line is the string that represent a line of the file.
*/

typedef struct	s_file_h
{
	struct s_file	*head;
	struct s_file	*tail;
}				t_file_h;

typedef struct	s_file
{
	size_t			id;
	char			*line;
	struct s_file_h	*header;
	struct s_file	*next;
	struct s_file	*prev;
}				t_file;

typedef struct	s_neig_h
{
	struct s_neig	*head;
	struct s_neig	*tail;
}				t_neig_h;

typedef struct	s_neig
{
	size_t			id;
	struct s_room	*room;
	struct s_neig_h	*header;
	struct s_neig	*next;
	struct s_neig	*prev;
}				t_neig;

/*
**		S_ROOM:
**	The s_room structure contains the informations of each room of the graph.
**		- The size_t id is the identifier of a given room.
**		- The int coor_x is the x position of the room.
**		- The int coor_y is the y position of the room.
**		- The char mod stores the modification of the room. A mod to 1 means
**		that the room is the start, and a mod to 2, the end.
**		- The char full defines if the room is full or not. 1: full, 0: empty
**		- The char *name corresponds to the name of the room.
**		- The struct s_neig_h is the header of the linked list of all the
**		neighbors of a given room.
*/

typedef struct	s_room_h
{
	struct s_room	*start;
	struct s_room	*end;
	struct s_room	*head;
	struct s_room	*tail;
}				t_room_h;

typedef struct	s_room
{
	size_t			id;
	int				coor_x;
	int				coor_y;
	char			mod;
	char			full;
	char			*name;
	struct s_neig_h	neig_h;
	struct s_room_h	*header;
	struct s_room	*parent;
	struct s_room	*next;
	struct s_room	*prev;
}				t_room;

typedef struct	s_path_h
{
	struct s_path	*head;
	struct s_path	*tail;
}				t_path_h;

typedef struct	s_path
{
	int				total;
	int				gen;
	int				len_path;
	int				gen_tmp;
	int				save;
	struct s_ptr	*ptr;
	struct s_path_h	*header;
	struct s_path	*next;
	struct s_path	*prev;
}				t_path;

typedef struct	s_ptr
{
	int				ant_id;
	struct s_room	*room;
	struct s_ptr	*next;
	struct s_ptr	*prev;
}				t_ptr;

/*
**		S_GRAPH:
**	The s_graph structure contains the informations of the ants and the rooms of
**	the source file.
**		- The ant_h structure is the header of the linked list of the ants.
**		- The room_h structure is the header of the linked list of the rooms.
*/

typedef struct	s_graph
{
	int				gen;
	int				best_gen;
	int				ant_members;
	int				nb_opts;
	short			opts;
	float			nb_tour;
	struct s_room_h	room_h;
	struct s_path_h	path_h;
}				t_graph;

typedef enum	e_mod
{
	LM_START,
	LM_END
}				t_mod;

typedef enum	e_opts
{
	LM_OPTS_C,
	LM_OPTS_L,
	LM_OPTS_P
}				t_opts;

#endif

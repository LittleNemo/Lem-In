/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:19:38 by lbrangie          #+#    #+#             */
/*   Updated: 2019/01/29 22:52:40 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include "libft.h"
# include "ft_printf.h"
# include "lem_in_defines.h"
# include "lem_in_typedefs.h"

void			lm_get_opts(char **av, t_graph *graph);
void			lm_error(int c, t_file_h *file_h, t_graph *graph, char **tab);

void			lm_set_file(t_file_h *file_h);
void			lm_get_file(t_file_h *file_h);
void			lm_del_file(t_file_h *file_h);

void			lm_set_remain(t_file_h *file_h, t_graph *graph);
void			lm_get_data(t_file_h *file_h, t_graph *graph);

t_file			*lm_get_ants(t_file_h *file_h, t_graph *graph);
void			lm_set_room(t_room_h *room_h);
t_file			*lm_get_room(t_file_h *file_h, t_graph *graph, t_file *tmp);
void			lm_del_room(t_room_h *room_h);
void			lm_set_neig(t_neig_h *neig_h);
t_file			*lm_get_neig(t_file_h *file_h, t_graph *graph, t_file *tmp);
void			lm_del_neig(t_neig_h *neig_h);

t_room			*lm_find_room(t_room_h *room_h, char *name);
t_room			*lm_find_start(t_room_h *room_h);
t_room			*lm_find_end(t_room_h *room_h);

int				lm_backward(t_room *back, t_ptr *queue, t_graph *graph);
int				lm_bfs(t_room *parent, t_ptr *queue, \
		t_ptr *curr, t_graph *graph);
void			lm_first_bfs(t_room *parent, t_ptr *queue, \
		t_file_h *file_h, t_graph *graph);
int				lm_solve(t_file_h *file_h, t_graph *graph);

t_path			*lm_new_path(t_ptr *curr, t_ptr *queue, \
		t_file_h *file_h, t_graph *graph);
int				lm_shin_lim(t_graph *graph);
int				create_paquet(t_path *path, t_path *head, int nb_path, int gen);
void			lm_save(t_graph *graph, int gen);
void			lm_recovery(t_graph *graph, int gen);
int				lm_sommes(t_path *path, int gen);
int				count_path(t_path *path, int gen);
void			len_path(t_path *path);
int				lm_simulation(t_graph *graph);

void			lm_set_path(t_path_h *path_h);
int				lm_create_path(t_ptr *curr, t_ptr *queue, t_graph *graph);
int				lm_create_to_end(t_ptr *curr, t_room *end, \
		t_ptr *queue, t_graph *graph);
void			lm_del_path(t_path_h *path_h);

void			lm_queue_push(t_ptr **queue, t_room *room, \
		t_file_h *file_h, t_graph *graph);
t_ptr			*lm_queue_pop(t_ptr **queue);
void			lm_del_queue(t_ptr *queue);

void			lm_purge_path(t_graph *graph);
void			lm_sort_path(t_graph *graph);
void			lm_move(t_graph *graph, int ant, int ant_end, int line);

int				lm_islock(t_room *room);
int				lm_ispath(t_room *room, t_graph *graph);
void			lm_reset_map(t_graph *graph, t_room *tmp_r);
int				lm_max_path(t_room_h *room_h);
void			lm_unfill_rooms(t_graph *graph);
t_ptr			*lm_go_to_end(t_path *path);
t_ptr			*lm_find_in_path(t_room *room, t_graph *graph);

void			lm_put_ants(t_graph *graph);
void			lm_put_room(t_room_h *room_h);
void			lm_put_file(t_file_h *file_h);
void			lm_put_path(t_path_h *path_h);

#endif

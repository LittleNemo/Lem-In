/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in_typedefs.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/03 16:29:19 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 13:43:56 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_TYPEDEFS_H
# define LEM_IN_TYPEDEFS_H

typedef struct	s_neib
{
	struct s_room	*room;
	struct s_neib	*next;
	struct s_neib	*prev;
}				t_neib;

typedef struct	s_conc
{
	unsigned int	id;
	struct s_room	*room_a;
	struct s_room	*room_b;
	struct s_conc	*next;
	struct s_conc	*prev;
}				t_conc;

typedef struct	s_room
{
	unsigned int	id;
	int				coord_x;
	int				coord_y;
	int				heat;
	char			mod;
	char			full;
	char			*name;
	struct s_neib	*neighbors;
	struct s_room	*next;
	struct s_room	*prev;
}				t_room;

typedef struct	s_ant
{
	unsigned int	id;
	struct s_room	*room;
	struct s_ant	*next;
	struct s_ant	*prev;
}				t_ant;

typedef struct	s_graph
{
	struct s_ant	*ants;
	struct s_room	*rooms;
	struct s_conc	*concs;
}				t_graph;

typedef struct	s_file
{
	unsigned int	id;
	char			*line;
	struct s_file	*next;
	struct s_file	*prev;
}				t_file;

typedef enum	e_mod
{
	LM_ROOM_S,
	LM_ROOM_E
}				t_mod;

#endif

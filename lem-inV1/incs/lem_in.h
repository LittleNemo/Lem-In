/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 16:15:11 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 13:26:36 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include <stdlib.h>
# include <stdio.h>
# include "../libs/libft/includes/libft.h"
# include "../libs/ft_printf/includes/ft_printf.h"
# include "lem_in_defines.h"
# include "lem_in_typedefs.h"

int				lm_error(int code, t_file **file, t_graph *graph, t_file *tmp);

void			lm_display_file(t_file *file);
void			lm_get_file(t_file **file);

t_file			*lm_get_concs(t_file **file, t_graph *graph, t_file *tmp);
t_file			*lm_get_rooms(t_file **file, t_graph *graph, t_file *tmp);
t_file			*lm_get_ants(t_file **file, t_graph *graph, t_file *tmp);
void			lm_get_data(t_file **file, t_graph *graph);

void			lm_check_data(t_file **file, t_graph *graph);
void			lm_set_remain(t_file **file, t_graph *graph);

void			lm_solve(t_graph *graph);

void			lm_free_ant(t_ant *ants);
void			lm_free_file(t_file *file);
void			lm_free_room(t_room *rooms);
void			lm_free_conc(t_conc *concs);
void			lm_free_graph(t_graph *graph);

t_room			*lm_find_room(t_graph *graph, char *room_name);
t_room			*lm_find_start(t_graph *graph);
t_room			*lm_find_end(t_graph *graph);
int				lm_heuristic(t_room *tmp, t_room *end);

#endif

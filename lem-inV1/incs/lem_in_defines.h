/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in_defines.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/03 16:45:36 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 15:48:52 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_DEFINES_H
# define LEM_IN_DEFINES_H

# ifndef LM_USAGE
#  define LM_USAGE			"usage: ./lem-in [< source_file]"
# endif

# ifndef LM_ERR_MALLOC
#  define LM_ERR_MALLOC		"Error (lem-in): malloc failed"
# endif

# ifndef LM_TEMPLATE
#  define LM_TEMPLATE		"%s\n      (line %u): '%s'\n"
# endif

# ifndef LM_ERR_EMPTY
#  define LM_ERR_EMPTY		"Error (lem-in): empty source file"
# endif
# ifndef LM_ERR_FORMAT
#  define LM_ERR_FORMAT		"Error (lem-in): line not well formated"
# endif

# ifndef LM_ERR_NO_ANT
#  define LM_ERR_NO_ANT		"Error (lem-in): no ant number found"
# endif

# ifndef LM_ERR_NEG_ANT1
#  define LM_ERR_NEG_ANT1	"Error (lem-in): invalid number of ant;"
# endif
# ifndef LM_ERR_NEG_ANT2
#  define LM_ERR_NEG_ANT2	" must be at least 1"
# endif
# ifndef LM_ERR_NEG_ANT
#  define LM_ERR_NEG_ANT	LM_ERR_NEG_ANT1 LM_ERR_NEG_ANT2
# endif

# ifndef LM_ERR_NO_END
#  define LM_ERR_NO_END		"Error (lem-in): end room not found"
# endif
# ifndef LM_ERR_NO_START
#  define LM_ERR_NO_START	"Error (lem-in): start room not found"
# endif
# ifndef LM_ERR_NO_ROOM
#  define LM_ERR_NO_ROOM	"Error (lem-in): no room found"
# endif
# ifndef LM_ERR_NO_PATH
#  define LM_ERR_NO_PATH	"Error (lem-in): no path from start room to end"
# endif

# ifndef LM_ERR_ROOM_AMB1
#  define LM_ERR_ROOM_AMB1	"Error (lem-in): room name can cause ambiguities;"
# endif
# ifndef LM_ERR_ROOM_AMB2
#  define LM_ERR_ROOM_AMB2	" please remove - or change name"
# endif
# ifndef LM_ERR_ROOM_AMB
#  define LM_ERR_ROOM_AMB	LM_ERR_ROOM_AMB1 LM_ERR_ROOM_AMB2
# endif
# ifndef LM_ERR_ROOM_DUP
#  define LM_ERR_ROOM_DUP	"Error (lem-in): this room already exist"
# endif
# ifndef LM_ERR_CON_UK
#  define LM_ERR_CON_UK		"Error (lem-in): a room does not exist"
# endif

# ifndef LM_ERR_UNKNOWN
#  define LM_ERR_UNKNOWN	"Error (lem-in): unknown"
# endif

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_freeing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 18:44:45 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 14:25:08 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_free_ant(t_ant *ants)
{
	t_ant			*tmp;

	while (ants)
	{
		tmp = ants;
		ants = ants->next;
		tmp->room = NULL;
		tmp->next = NULL;
		tmp->prev = NULL;
		free(tmp);
	}
}

void			lm_free_file(t_file *file)
{
	t_file			*tmp;

	while (file)
	{
		tmp = file;
		file = file->next;
		ft_strdel(&tmp->line);
		tmp->next = NULL;
		tmp->prev = NULL;
		free(tmp);
	}
}

void			lm_free_room(t_room *rooms)
{
	t_room			*tmp;
	t_neib			*tmp_n;

	while (rooms)
	{
		tmp = rooms;
		rooms = rooms->next;
		ft_strdel(&tmp->name);
		while (tmp->neighbors)
		{
			tmp_n = tmp->neighbors;
			tmp->neighbors = tmp->neighbors->next;
			tmp_n->room = NULL;
			tmp_n->next = NULL;
			tmp_n->prev = NULL;
			free(tmp_n);
		}
		tmp->next = NULL;
		tmp->prev = NULL;
		free(tmp);
	}
}

void			lm_free_conc(t_conc *concs)
{
	t_conc			*tmp;

	while (concs)
	{
		tmp = concs;
		concs = concs->next;
		tmp->room_a = NULL;
		tmp->room_b = NULL;
		tmp->next = NULL;
		tmp->prev = NULL;
		free(tmp);
	}
}

void			lm_free_graph(t_graph *graph)
{
	lm_free_room(graph->rooms);
	lm_free_conc(graph->concs);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_set_remain.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 15:35:45 by lbrangie          #+#    #+#             */
/*   Updated: 2018/11/14 13:44:26 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_set_start(t_graph *graph, t_room *start)
{
	t_ant			*tmp;

	tmp = graph->ants;
	while (tmp)
	{
		tmp->room = start;
		tmp = tmp->next;
	}
	lm_find_start(graph)->full = 1;
}

static void		lm_neib_update(t_file **file, t_graph *graph, \
				t_room *room, t_room *neib)
{
	t_neib			*fresh;
	t_neib			*tmp;

	if (!(fresh = (t_neib *)malloc(sizeof(*fresh))))
		lm_error(2, file, graph, NULL);
	fresh->room = neib;
	fresh->next = NULL;
	tmp = room->neighbors;
	if (!room->neighbors)
	{
		fresh->prev = NULL;
		room->neighbors = fresh;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = fresh;
	fresh->prev = tmp;
}

static void		lm_set_neighbors(t_file **file, t_graph *graph)
{
	t_conc			*tmp;

	tmp = graph->concs;
	while (tmp)
	{
		lm_neib_update(file, graph, \
				lm_find_room(graph, tmp->room_a->name), tmp->room_b);
		lm_neib_update(file, graph, \
				lm_find_room(graph, tmp->room_b->name), tmp->room_a);
		tmp = tmp->next;
	}
}

static void		lm_set_heat(t_room *room, t_room *prev_n, int heat)
{
	t_neib			*neighbor;

	neighbor = room->neighbors;
	while (neighbor && neighbor->room->heat != -1 && \
		neighbor->room->heat <= heat)
		neighbor = neighbor->next;
	if (room->heat >= heat || room->heat == -1)
		room->heat = heat;
	while (neighbor)
	{
		if (neighbor->room != prev_n)
			lm_set_heat(neighbor->room, room, heat + 1);
		neighbor = neighbor->next;
	}
}

void			lm_set_remain(t_file **file, t_graph *graph)
{
	t_room			*tmp;

	lm_check_data(file, graph);
	lm_set_start(graph, lm_find_start(graph));
	tmp = NULL;
	tmp = graph->rooms;
	while (tmp)
	{
		tmp->heat = -1;
		tmp = tmp->next;
	}
	lm_set_neighbors(file, graph);
	lm_set_heat(lm_find_end(graph), NULL, 0);
	if (lm_find_start(graph)->heat == -1)
		lm_error(13, file, graph, NULL);
}

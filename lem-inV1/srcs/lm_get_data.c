/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_data.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 18:03:28 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/25 17:56:07 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_get_data(t_file **file, t_graph *graph)
{
	t_file			*tmp;

	tmp = *file;
	while (tmp->line && tmp->line[0] == '#' && tmp->line[1] != '#')
		tmp = tmp->next;
	if (!tmp->line[0])
		return ;
	if (!(tmp = lm_get_ants(file, graph, tmp)))
		return ;
	if (!(tmp = lm_get_rooms(file, graph, tmp)))
		return ;
	if ((tmp = lm_get_concs(file, graph, tmp)))
		return ;
}

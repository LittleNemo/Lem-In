/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_check_data.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 13:54:33 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 18:55:52 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_check_data(t_file **file, t_graph *graph)
{
	if (!graph->rooms)
		lm_error(9, file, graph, NULL);
	if (!lm_find_start(graph))
		lm_error(8, file, graph, NULL);
	if (!lm_find_end(graph))
		lm_error(7, file, graph, NULL);
}

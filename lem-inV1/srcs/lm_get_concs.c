/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_concs.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 15:10:32 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 18:44:35 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_concs_update(t_file **file, t_graph *graph, char **tab)
{
	t_conc				*fresh;
	t_conc				*tmp;
	static unsigned int	id;

	if (!(fresh = (t_conc *)malloc(sizeof(*fresh))))
		lm_error(2, file, graph, NULL);
	fresh->id = id++;
	fresh->room_a = lm_find_room(graph, tab[0]);
	fresh->room_b = lm_find_room(graph, tab[1]);
	fresh->next = NULL;
	ft_freetab((void **)tab);
	tmp = graph->concs;
	if (!graph->concs)
	{
		fresh->prev = NULL;
		graph->concs = fresh;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = fresh;
	fresh->prev = tmp;
}

static int		lm_check_line_concs(t_file **file, t_graph *graph, t_file *tmp)
{
	char			**tab;

	if (!(tab = ft_strsplit(tmp->line, '-')))
		lm_error(2, file, graph, tmp);
	if (ft_tablen(tab) != 2)
	{
		ft_freetab((void **)tab);
		return (1);
	}
	if (!lm_find_room(graph, tab[0]) || !lm_find_room(graph, tab[1]))
	{
		ft_freetab((void **)tab);
		lm_error(12, file, graph, tmp);
	}
	lm_concs_update(file, graph, tab);
	return (0);
}

t_file			*lm_get_concs(t_file **file, t_graph *graph, t_file *tmp)
{
	while (tmp && (ft_strchr(tmp->line, '-') || tmp->line[0] == '#'))
	{
		if ((tmp->line[0] != '#') && lm_check_line_concs(file, graph, tmp))
			return (NULL);
		tmp = tmp->next;
	}
	return (tmp);
}

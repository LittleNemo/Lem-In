/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 16:43:03 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 19:47:32 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_error_next(int code, t_file *tmp)
{
	if (code == 5)
		ft_fprintf(stderr, LM_TEMPLATE, LM_ERR_NEG_ANT, tmp->id, tmp->line);
	else if (code == 6)
		ft_fprintf(stderr, "%s\n", LM_ERR_NO_ANT);
	else if (code == 7)
		ft_fprintf(stderr, "%s\n", LM_ERR_NO_END);
	else if (code == 8)
		ft_fprintf(stderr, "%s\n", LM_ERR_NO_START);
	else if (code == 9)
		ft_fprintf(stderr, "%s\n", LM_ERR_NO_ROOM);
	else if (code == 10)
		ft_fprintf(stderr, LM_TEMPLATE, LM_ERR_ROOM_AMB, tmp->id, tmp->line);
	else if (code == 11)
		ft_fprintf(stderr, LM_TEMPLATE, LM_ERR_ROOM_DUP, tmp->id, tmp->line);
	else if (code == 12)
		ft_fprintf(stderr, LM_TEMPLATE, LM_ERR_CON_UK, tmp->id, tmp->line);
	else if (code == 13)
		ft_fprintf(stderr, "%s\n", LM_ERR_NO_PATH);
	else
		ft_fprintf(stderr, "%s\n", LM_ERR_UNKNOWN);
}

int				lm_error(int code, t_file **file, t_graph *graph, t_file *tmp)
{
	if (code == 0)
		return (0);
	else if (code == 1)
	{
		ft_fprintf(stderr, "%s\n", LM_USAGE);
		exit(EXIT_FAILURE);
	}
	else if (code == 2)
		ft_fprintf(stderr, "%s\n", LM_ERR_MALLOC);
	else if (code == 3)
	{
		ft_fprintf(stderr, "%s\n", LM_ERR_EMPTY);
		exit(EXIT_FAILURE);
	}
	else if (code == 4)
		ft_fprintf(stderr, LM_TEMPLATE, LM_ERR_FORMAT, tmp->id, tmp->line);
	else
		lm_error_next(code, tmp);
	lm_free_file(*file);
	lm_free_ant(graph->ants);
	lm_free_graph(graph);
	exit(EXIT_FAILURE);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_file.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 15:09:07 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/25 18:41:27 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_get_file_update(t_file **file, char *line, unsigned int id)
{
	t_file			*fresh;
	t_file			*tmp;

	if (!(fresh = (t_file *)malloc(sizeof(*fresh))))
		lm_error(2, NULL, NULL, NULL);
	fresh->id = id;
	fresh->line = ft_strdup(line);
	fresh->next = NULL;
	tmp = *file;
	if (!*file)
	{
		fresh->prev = NULL;
		*file = fresh;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = fresh;
	fresh->prev = tmp;
}

void			lm_get_file(t_file **file)
{
	unsigned int	id;
	char			*line;

	id = 1;
	while (get_next_line(stdin->_file, &line))
	{
		lm_get_file_update(file, line, id++);
		ft_strdel(&line);
	}
	ft_strdel(&line);
	if (!*file)
		lm_error(3, NULL, NULL, NULL);
}

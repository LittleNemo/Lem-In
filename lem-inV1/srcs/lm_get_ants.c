/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_ants.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 16:22:04 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 19:00:42 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_ants_update(t_file **file, t_graph *graph, unsigned int id)
{
	t_ant			*fresh;
	t_ant			*tmp;

	if (!(fresh = (t_ant *)malloc(sizeof(*fresh))))
		lm_error(2, file, graph, NULL);
	fresh->id = id;
	fresh->next = NULL;
	tmp = graph->ants;
	if (!graph->ants)
	{
		fresh->prev = NULL;
		graph->ants = fresh;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = fresh;
	fresh->prev = tmp;
}

t_file			*lm_get_ants(t_file **file, t_graph *graph, t_file *tmp)
{
	unsigned int	id;
	int				value;
	int				i;

	i = -1;
	while (tmp->line[++i])
		if (!(ft_isdigit(tmp->line[i]) || tmp->line[i] == '-'))
			lm_error(4, file, graph, tmp);
	value = ft_atoi(tmp->line);
	if (ft_strlen(tmp->line) != (ft_numlen(value) + ft_isneg(value)))
		lm_error(4, file, graph, tmp);
	if (value <= 0)
		lm_error(5, file, graph, tmp);
	id = 1;
	while ((int)id < value + 1)
		lm_ants_update(file, graph, id++);
	tmp = tmp->next;
	return (tmp);
}

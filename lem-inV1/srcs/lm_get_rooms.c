/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_get_rooms.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/17 13:52:34 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/26 18:53:37 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void		lm_room_update(t_file **file, t_graph *graph, \
				char **tab, char mod)
{
	t_room				*fresh;
	t_room				*tmp;
	static unsigned int	id;

	if (!(fresh = (t_room *)malloc(sizeof(*fresh))))
		lm_error(2, file, graph, NULL);
	fresh->id = id++;
	fresh->coord_x = ft_atoi(tab[1]);
	fresh->coord_y = ft_atoi(tab[2]);
	fresh->mod = mod;
	fresh->full = 0;
	fresh->name = ft_strdup(tab[0]);
	fresh->neighbors = NULL;
	fresh->next = NULL;
	tmp = graph->rooms;
	if (!graph->rooms)
	{
		fresh->prev = NULL;
		graph->rooms = fresh;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = fresh;
	fresh->prev = tmp;
}

static int		lm_check_str(t_graph *graph, char *str, int code)
{
	t_room			*tmp;
	int				i;

	if (code == 1)
	{
		tmp = graph->rooms;
		while (tmp)
		{
			if (ft_strequ(str, tmp->name))
				return (1);
			tmp = tmp->next;
		}
		return (0);
	}
	i = -1;
	while (str[++i])
		if (!(ft_isdigit(str[i]) || str[i] == '-'))
			return (0);
	return (1);
}

static void		lm_reset_mod(t_graph *graph, char mod)
{
	t_room			*tmp;

	tmp = graph->rooms;
	while (tmp)
	{
		if (tmp->mod == mod)
			tmp->mod = 0;
		tmp = tmp->next;
	}
}

static int		lm_check_line_room(t_file **file, t_graph *graph, \
				t_file *tmp, char mod)
{
	char			**tab;

	if (!(tab = ft_strsplit(tmp->line, ' ')))
		lm_error(2, file, graph, tmp);
	if (ft_tablen(tab) != 3 || !lm_check_str(graph, tab[1], 2) || \
		!lm_check_str(graph, tab[2], 2) || tab[0][0] == 'L')
	{
		ft_freetab((void **)tab);
		return (1);
	}
	if (ft_strchr(tab[0], '-'))
	{
		ft_freetab((void **)tab);
		lm_error(10, file, graph, tmp);
	}
	if (lm_check_str(graph, tab[0], 1))
	{
		ft_freetab((void **)tab);
		lm_error(11, file, graph, tmp);
	}
	if (mod)
		lm_reset_mod(graph, mod);
	lm_room_update(file, graph, tab, mod);
	ft_freetab((void **)tab);
	return (0);
}

t_file			*lm_get_rooms(t_file **file, t_graph *graph, t_file *tmp)
{
	while (tmp && (ft_strchr(tmp->line, ' ') || tmp->line[0] == '#'))
	{
		if (ft_strequ(tmp->line, "##start"))
		{
			if (lm_check_line_room(file, graph, tmp->next, 1 << LM_ROOM_S))
				return (NULL);
			tmp = tmp->next;
		}
		else if (ft_strequ(tmp->line, "##end"))
		{
			if (lm_check_line_room(file, graph, tmp->next, 1 << LM_ROOM_E))
				return (NULL);
			tmp = tmp->next;
		}
		else if (tmp->line[0] != '#')
			if (lm_check_line_room(file, graph, tmp, 0))
				return (NULL);
		tmp = tmp->next;
	}
	lm_check_data(file, graph);
	return (tmp);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 15:03:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/10/25 16:19:51 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_room			*lm_find_room(t_graph *graph, char *room_name)
{
	t_room			*tmp;

	tmp = graph->rooms;
	while (tmp)
	{
		if (ft_strequ(tmp->name, room_name))
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

t_room			*lm_find_start(t_graph *graph)
{
	t_room			*tmp;

	tmp = graph->rooms;
	while (tmp)
	{
		if (tmp->mod & (1 << LM_ROOM_S))
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

t_room			*lm_find_end(t_graph *graph)
{
	t_room			*tmp;

	tmp = graph->rooms;
	while (tmp)
	{
		if (tmp->mod & (1 << LM_ROOM_E))
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

int				lm_heuristic(t_room *tmp, t_room *end)
{
	int				a;
	int				b;

	a = tmp->coord_x - end->coord_x;
	b = tmp->coord_y - end->coord_y;
	return (ft_abs(a + b));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 16:21:17 by lbrangie          #+#    #+#             */
/*   Updated: 2018/11/12 17:20:25 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			lm_display_file(t_file *file)
{
	while (file)
	{
		ft_printf("%s\n", file->line);
		file = file->next;
	}
	ft_printf("\n");
}

static void		lm_set_null(t_file **file, t_graph *graph)
{
	*file = NULL;
	graph->ants = NULL;
	graph->rooms = NULL;
	graph->concs = NULL;
}

int				main(int ac, char **av)
{
	t_file			*file;
	t_graph			graph;

	(void)av;
	lm_error(ac - 1, NULL, NULL, NULL);
	lm_set_null(&file, &graph);
	lm_get_file(&file);
	lm_get_data(&file, &graph);
	lm_set_remain(&file, &graph);
	lm_display_file(file);
	lm_free_file(file);
	lm_solve(&graph);
	lm_free_ant(graph.ants);
	lm_free_graph(&graph);
	return (0);
}

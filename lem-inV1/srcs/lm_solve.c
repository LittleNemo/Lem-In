/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lm_solve.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 17:32:01 by lbrangie          #+#    #+#             */
/*   Updated: 2018/11/14 13:44:21 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int		lm_ant_isnot_end(t_graph *graph)
{
	t_ant			*tmp;

	tmp = graph->ants;
	while (tmp)
	{
		if (tmp->room->mod != (1 << LM_ROOM_E))
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

static void		lm_move(t_ant *ant, t_room *coolest)
{
	ant->room->full = 0;
	ant->room = coolest;
	ant->room->full = 1;
	ft_printf("L%u-%s ", ant->id, ant->room->name);
}

static void		lm_solve_loop(t_ant *ant, t_room *coolest, \
				t_neib *neighbor, int new)
{
	while (ant)
	{
		new = 0;
		coolest = ant->room;
		neighbor = ant->room->neighbors;
		while (neighbor)
		{
			if (((neighbor->room->mod & (1 << LM_ROOM_E)) || \
				!neighbor->room->full) && \
				neighbor->room->heat <= coolest->heat && \
				!(neighbor->room->mod & (1 << LM_ROOM_S)))
			{
				coolest = neighbor->room;
				new = 1;
			}
			neighbor = neighbor->next;
		}
		if (new)
			lm_move(ant, coolest);
		ant = ant->next;
	}
}

void			lm_solve(t_graph *graph)
{
	t_ant			*ant;
	t_room			*coolest;
	t_neib			*neighbor;
	int				new;

	coolest = NULL;
	neighbor = NULL;
	new = 0;
	while (!lm_ant_isnot_end(graph))
	{
		ant = graph->ants;
		lm_solve_loop(ant, coolest, neighbor, new);
		ft_printf("\n");
	}
}
